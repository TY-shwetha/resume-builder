import React from 'react'

function CustomTextfield(props) {
  return (
    <input 
    {...props}
    class="my-5 w-3/4  ml-8 px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
    type="text"
    autoComplete='off'

    />
  )
}

export default CustomTextfield