import * as React from "react";
import { Button } from "@mui/material";
import {
  FacebookFilled,
  GithubFilled,
  InstagramOutlined,
  LinkedinFilled,
  MailFilled,
  PhoneFilled,
  TwitterCircleFilled,
} from "@ant-design/icons/lib/icons";
import CustomTextfield from "./customComponents/CustomTextfield";
import { useFormik } from "formik";
import { Link } from "react-router-dom";

export default function PersonalDetailes() {
  const [arr, setArr] = React.useState([]);
  const initialValues = {
    firstName: "",
    email: "",
    website: "",
    facebook: "",
    linkedin: "",
    lastName: "",
    phoneNum: "",
    github: "",
    twitter: "",
    instagram: "",
  };

  const onSubmit = (values) => {
    console.log(values);
    setArr((pre) => [...pre, values]);
  };

  React.useEffect(() => {
    console.log(arr);
  }, [arr]);

  const formik = useFormik({
    initialValues,
    onSubmit,
  });

  const handleReset = () => {
    Array.from(document.querySelectorAll("input")).forEach(
      (input) => (input.value = "")
    );
    setArr([]);
  };

  return (
      <>
{/* <div className="bg-blue-300 shadow-md h-8"><h1>hello</h1></div> */}
    
    <form onSubmit={formik.handleSubmit}>
      <div className="flex justify-center items-center h-[100vh]">
        <div className="shadow-md  w-1/2">
          <div className="shadow-md my-5 text-3xl  p-2  ">
            Personal Detailes{" "}
          </div>
          <div className="grid grid-cols-2 ">
            <div className="grid-cols-1">
              <CustomTextfield
                placeholder="First Name"
                name="firstName"
                {...formik.getFieldProps("firstName")}
              />
              <div className="relative">
                <CustomTextfield
                  placeholder="you@example.com"
                  name="email"
                  {...formik.getFieldProps("email")}
                />
                <MailFilled className="absolute top-[0.7rem] left-[73%] " />
              </div>
              <CustomTextfield
                placeholder="Your website"
                name="website"
                {...formik.getFieldProps("website")}
              />
              <div className="relative">
                <CustomTextfield
                  placeholder="Facebook"
                  name="facebook"
                  {...formik.getFieldProps("facebook")}
                />
                <FacebookFilled className="absolute top-[0.7rem] left-[73%] " />
              </div>
              <div className="relative">
                <CustomTextfield
                  placeholder="Linkedin"
                  name="linkedin"
                  {...formik.getFieldProps("linkedin")}
                />
                <LinkedinFilled className="absolute top-[0.7rem] left-[73%] " />
              </div>
            </div>

            <div className="grid-cols-1">
              <CustomTextfield
                placeholder="Last Name"
                name="lastName"
                {...formik.getFieldProps("lastName")}
              />
              <div className="relative">
                <CustomTextfield
                  placeholder="Phone Number"
                  name="phoneNum"
                  {...formik.getFieldProps("phoneNum")}
                />
                <PhoneFilled className="absolute top-[0.7rem] left-[73%] " />
              </div>

              <div className="relative">
                <CustomTextfield
                  placeholder="GitHub"
                  name="github"
                  {...formik.getFieldProps("github")}
                />
                <GithubFilled className="absolute top-[0.7rem] left-[73%] " />
              </div>

              <div className="relative">
                <CustomTextfield
                  placeholder="Twitter"
                  name="twitter"
                  {...formik.getFieldProps("twitter")}
                />
                <TwitterCircleFilled className="absolute top-[0.7rem] left-[73%] " />
              </div>

              <div className="relative">
                <CustomTextfield
                  placeholder="instagram"
                  name="instagram"
                  {...formik.getFieldProps("instagram")}
                />
                <InstagramOutlined className="absolute top-[0.7rem] left-[73%] " />
              </div>
            </div>
          </div>
          <div className="m-4 flex justify-end">
            <span className="mr-2">
              <Button variant="contained" onClick={handleReset}>
                Reset
              </Button>
            </span>
            <Link to='/educationalDetailes'>
            <Button type="submit" variant="contained">
              Next
            </Button>
            </Link>
          </div>
        </div>
      </div>
    </form>
    </>

  );
}
