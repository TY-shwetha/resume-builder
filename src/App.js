import logo from './logo.svg';
import './App.css';

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import PersonalDetailes from './components/PersonalDetailes';
import EducationalDetailes from './components/EducationalDetailes';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
    <Routes>

     <Route path='/' element = {<PersonalDetailes/>}/>
     <Route path='/educationalDetailes' element = {<EducationalDetailes/>}/>

    </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
